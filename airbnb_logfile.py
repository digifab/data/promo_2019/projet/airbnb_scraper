#!/usr/bin/python3
# ===========================================================
#  Save collected data and their metadata in separate files
# ===========================================================
import errno
import csv
import json
import logging
import os
from datetime import date, datetime

from airbnb_config import ABConfig

logger = logging.getLogger()


def saveasjson(data_var, step, save_mode="a", ind=4):
    """
    Used for certification to save a copy of the data's states in a folder.
    The first parameter is the variable storing
    the data in memory, the secondone is the name of the
    data's state (eg. raw, cleaned, processed, ...)
    """
    if logger.isEnabledFor(logging.DEBUG):
        today = date.today()
        counter = 0
        if save_mode == "w":
            filename = "data/" + step + "/" + \
                    step + "_" + str(today) + "_{}.json"
        else:
            filename = "data/" + step + "/" + \
                    step + "_" + str(today) + ".json"
        directory = os.path.dirname(filename)
        if not os.path.exists(directory):
            try:
                os.makedirs(directory)
            except OSError as error:
                if error.errno != errno.EEXIST:
                    raise
        if save_mode == "w":
            while os.path.isfile(filename.format(counter)):
                counter += 1
            filename = filename.format(counter)
        json_file = open(filename, mode=save_mode, encoding="utf-8")
        json_file.write(json.dumps(data_var, indent=ind, sort_keys=True) + "\n")
        json_file.close()


def saveasmeta(data_name, format="json"):
    """
    Make a metadata of saved files
    """
    if logger.isEnabledFor(logging.DEBUG):
        today = date.today()

        desc_raw = "Raw data"
        desc_processed = "Listings saved from the raw data"
        desc_clean_inserted = \
            "Data cleaned before insertion in the database"
        desc_clean_updated = \
            "Data updated in the database"
        desc_clean_deleted = \
            "Data maked as deleted in the database"
        desc_final = "Data extracted from the database"
        source = \
            "https://www.airbnb.fr/api/v2/explore_tabs"
        data_date = datetime.now().strftime("%d/%m/%Y %H:%M:%S")

        place = "data/" + data_name + "/" + data_name + \
            "_" + str(today) + "." + format
        saveto = "metadata/metadata_" + str(today) + ".csv"
        directory = os.path.dirname(saveto)
        if not os.path.exists(directory):
            try:
                os.makedirs(directory)
            except OSError as error:
                if error.errno != errno.EEXIST:
                    raise

        with open(saveto, mode='a') as csv_file:
            fieldnames = ['Step', 'Description', 'Source', 'Destination', 'Date']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            # writer.writeheader()
            if data_name == "raw":
                writer.writerow({'Step': '1', 'Description': desc_raw, 'Source': source, 'Destination': place, 'Date': data_date})
            elif data_name == "processed":
                writer.writerow({'Step': '2', 'Description': desc_processed, 'Source': source, 'Destination': place, 'Date': data_date})
            elif data_name == "clean_inserted":
                writer.writerow({'Step': '3', 'Description': desc_clean_inserted, 'Source': source, 'Destination': place, 'Date': data_date})
            elif data_name == "clean_updated":
                writer.writerow({'Step': '3.1', 'Description': desc_clean_updated, 'Source': source, 'Destination': place, 'Date': data_date})
            elif data_name == "clean_deleted":
                writer.writerow({'Step': '3.2', 'Description': desc_clean_deleted, 'Source': source, 'Destination': place, 'Date': data_date})
            elif data_name == "final":
                writer.writerow({'Step': '4', 'Description': desc_final, 'Source': source, 'Destination': place, 'Date': data_date})

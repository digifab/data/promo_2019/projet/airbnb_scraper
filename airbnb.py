#!/usr/bin/python3
# ==============================================================
# Scraper Airbnb, pour l'analyse des locations (listings)
# English translation in progress...
#
# function naming conventions:
#   ws_get = get from web site
#   db_get = get from database
#   db_add = add to the database
#
# function name conventions:
#   add = add to database
#   display = open a browser and show
#   list = get from database and print
#   print = get from web site and print
#
# (Thanks to zedr : https://github.com/zedr/clean-code-python)
# (Cleaning method : https://stackoverflow.com/a/55389369)
# ==============================================================
import argparse
import logging
import sys
import webbrowser

import psycopg2
import psycopg2.errorcodes

from airbnb_config import ABConfig
from airbnb_scrape import ABScrapeByBoundingBox

SCRIPT_VERSION_NUMBER = "1.20"


# logging = logging.getLogger()


def list_search_area_info(config, search_area):
    """
    Print a list of the search areas in the database to stdout.
    """
    try:
        conn = config.connect()
        cur = conn.cursor()
        cur.execute("""
                select search_area_id
                from search_area where name=%s
                """, (search_area,))
        result_set = cur.fetchall()
        cur.close()
        count = len(result_set)
        if count == 1:
            print("\nThere is one search area called",
                  str(search_area),
                  "in the database.")
        elif count > 1:
            print("\nThere are", str(count),
                  "cities called", str(search_area),
                  "in the database.")
        elif count < 1:
            print("\nThere are no cities called",
                  str(search_area),
                  "in the database.")
            sys.exit()
        sql_search_area = """select count(*) from search_area
        where search_area_id = %s"""
        for result in result_set:
            search_area_id = result[0]
            cur = conn.cursor()
            cur.execute(sql_search_area, (search_area_id,))
            count = cur.fetchone()[0]
            cur.close()
            print("\t" + str(count) + " Airbnb cities.")
    except psycopg2.Error as pge:
        logging.error(pge.pgerror)
        logging.error("Error code %s", pge.pgcode)
        logging.error("Diagnostics %s", pge.diag.message_primary)
        cur.close()
        conn.rollback()
        raise
    except Exception:
        logging.error("Failed to list search area info")
        raise


def list_scrapes(config):
    """
    Print a list of the scrapes in the database to stdout.
    """
    try:
        conn = config.connect()
        cur = conn.cursor()
        cur.execute("""
            select scrape_id, to_char(scrape_date, 'YYYY-Mon-DD'),
                    scrape_description, search_area_id, status
            from scrape
            where scrape_date is not null
            and status is not null
            and scrape_description is not null
            order by scrape_id asc""")
        result_set = cur.fetchall()
        if result_set:
            template = "| {0:3} | {1:>12} | {2:>50} | {3:3} | {4:3} |"
            print(template.format("ID", "Date", "Description", "SA", "status"))
            for scrape in result_set:
                (scrape_id, scrape_date, desc, sa_id, status) = scrape
                print(template.format(scrape_id,
                                      scrape_date, desc, sa_id, status))
    except Exception:
        logging.error("Cannot list scrapes.")
        raise


def db_ping(config):
    """
    Test database connectivity, and print success or failure.
    """
    try:
        conn = config.connect()
        if conn is not None:
            print("Connection test succeeded: {db_name}@{db_host}"
                  .format(db_name=config.DB_NAME, db_host=config.DB_HOST))
        else:
            print("Connection test failed")
    except Exception:
        logging.exception("Connection test failed")


def db_add_scrape(config, search_area):
    """
    Add a scrape entry to the database, so the scrape can be run.
    Also returns the scrape_id, in case it is to be used..
    """
    try:
        conn = config.connect()
        cur = conn.cursor()
        # Add an entry into the scrape table, and get the scrape_id
        sql = """
        insert into scrape (scrape_description, search_area_id)
        select (name || ' (' || current_date || ')') as scrape_description,
        search_area_id
        from search_area
        where name = %s
        returning scrape_id"""
        cur.execute(sql, (search_area,))
        scrape_id = cur.fetchone()[0]

        # Get and print the scrape entry
        cur.execute("""select scrape_id, scrape_date,
        scrape_description, search_area_id
        from scrape where scrape_id = %s""", (scrape_id,))
        (scrape_id,
         scrape_date,
         scrape_description,
         search_area_id) = cur.fetchone()
        conn.commit()
        cur.close()
        print("\nScrape added:\n"
              + "\n\tscrape_id=" + str(scrape_id)
              + "\n\tscrape_date=" + str(scrape_date)
              + "\n\tscrape_description=" + scrape_description
              + "\n\tsearch_area_id=" + str(search_area_id))
        return scrape_id
    except Exception:
        logging.error("Failed to add scrape for %s", search_area)
        raise


def db_delete_scrape(config, scrape_id):
    """
    Delete the listings and progress for a scrape from the database.
    Set the scrape to "incomplete" in the scrape table.
    """
    question = "Are you sure you want to delete listings for scrape {}? [y/N] ".format(
        scrape_id)
    sys.stdout.write(question)
    choice = input().lower()
    if choice != "y":
        print("Cancelling the request.")
        return
    try:
        conn = config.connect()
        cur = conn.cursor()
        # Delete the listings from the room table
        sql = """
        delete from room where scrape_id = %s
        """
        cur.execute(sql, (scrape_id,))
        print("{} listings deleted from 'room' table".format(cur.rowcount))

        # Delete the entry from the progress log table
        sql = """
        delete from scrape_progress_log_bb where scrape_id = %s
        """
        cur.execute(sql, (scrape_id,))
        # No need to report: it's just a log table

        # Update the scrape entry
        sql = """
        update scrape
        set status = 0, scrape_date = NULL
        where scrape_id = %s
        """
        cur.execute(sql, (scrape_id,))
        if cur.rowcount == 1:
            print("Scrape entry updated")
        else:
            print("Warning: {} scrape entries updated".format(cur.rowcount))
        conn.commit()
        cur.close()
    except Exception:
        logging.error("Failed to delete scrape for %s", scrape_id)
        raise


def db_add_search_area(config, search_area):
    """
    Add a search_area to the database.
    """
    try:
        logging.info("Adding search_area to database as new search area")
        # Add the search_area to the database anyway
        conn = config.connect()
        cur = conn.cursor()
        # check if it exists
        sql = """
        select name
        from search_area
        where name = %s"""
        cur.execute(sql, (search_area,))
        if cur.fetchone() is not None:
            print("Area name already exists: {}".format(search_area))
            return True
        # Compute an abbreviation, which is optional and can be used
        # as a suffix for search_area views (based on a shapefile)
        # The abbreviation is lower case, has no whitespace, is 10 characters
        # or less, and does not end with a whitespace character
        # (translated as an underscore)
        abbreviation = search_area.lower()[:10].replace(" ", "_")
        while abbreviation[-1] == "_":
            abbreviation = abbreviation[:-1]

        # Insert the search_area into the table
        sql = """insert into search_area (name, abbreviation)
        values (%s, %s)"""
        cur.execute(sql, (search_area, abbreviation,))
        sql = """select
        currval('search_area_search_area_id_seq')
        """
        cur.execute(sql, ())
        search_area_id = cur.fetchone()[0]
        cur.close()
        conn.commit()
        print("Search area {} added: search_area_id = {}"
              .format(search_area, search_area_id))
        print("Before searching, update the row to add a bounding box, using SQL.")
        print(
            "I use coordinates from http://www.mapdevelopers.com/geocode_bounding_box.php.")
        print("The update statement to use is:")
        print("\n\tUPDATE search_area")
        print("\tSET bb_n_lat = ?, bb_s_lat = ?, bb_e_lng = ?, bb_w_lng = ?")
        print("\tWHERE search_area_id = {}".format(search_area_id))
        print("\nThis program does not provide a way to do this update automatically.")

    except Exception:
        print("Error adding search area to database")
        raise


def display_room(config, room_id):
    """
    Open a web browser and show the listing page for a room.
    GDPR: to demonstrate the need to anonymize the room_id
    """
    webbrowser.open(config.URL_ROOM_ROOT + str(room_id))


def display_host(config, host_id):
    """
    Open a web browser and show the user page for a host.
    GDPR: to demonstrate the need to anonymize the host_id
    """
    webbrowser.open(config.URL_HOST_ROOT + str(host_id))


def parse_args():
    """
    Read and parse command-line arguments
    """
    parser = argparse.ArgumentParser(
        description='Manage a database of Airbnb listings.',
        usage='%(prog)s [options]')
    parser.add_argument("-v", "--verbose",
                        action="store_true", default=True,
                        help="""write verbose (debug) output to the log file""")
    parser.add_argument("-c", "--config_file",
                        metavar="config_file", action="store", default=None,
                        help="""explicitly set configuration file, instead of
                        using the default <username>.config""")
    # Only one argument!
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-asa', '--addsearcharea',
                       metavar='search_area', action='store', default=False,
                       help="""add a search area to the database. A search area
                       is typically a city, but may be a bigger zone.""")
    group.add_argument('-as', '--add_scrape',
                       metavar='search_area', type=str,
                       help="""add a scrape entry to the database,
                       for search_area""")
    group.add_argument('-dbp', '--dbping',
                       action='store_true', default=False,
                       help='Test the database connection')
    group.add_argument('-dh', '--displayhost',
                       metavar='host_id', type=int,
                       help='display web page for host_id in browser')
    group.add_argument('-dr', '--displayroom',
                       metavar='room_id', type=int,
                       help='display web page for room_id in browser')
    group.add_argument('-ds', '--delete_scrape',
                       metavar='scrape_id', type=int,
                       help="""delete a scrape from the database, with its
                       listings""")
    group.add_argument('-lsa', '--listsearcharea',
                       metavar='search_area', type=str,
                       help="""list information about this search area
                       from the database""")
    group.add_argument('-ls', '--listscrapes',
                       action='store_true', default=False,
                       help='list the scrapes in the database')
    group.add_argument('-psb', '--printsearch_by_bounding_box',
                       metavar='scrape_id', type=int,
                       help="""print first page of search information
                       for scrape from the Airbnb web site,
                       by bounding_box""")
    group.add_argument('-sb', '--search_by_bounding_box',
                       metavar='scrape_id', type=int,
                       help="""search for rooms using scrape scrape_id,
                       by bounding box
                       """)
    group.add_argument('-asb', '--add_and_search_by_bounding_box',
                       metavar='search_area', type=str,
                       help="""add a scrape for search_area and search ,
                       by bounding box
                       """)
    group.add_argument('-V', '--version',
                       action='version',
                       version='%(prog)s, version ' +
                               str(SCRIPT_VERSION_NUMBER))
    group.add_argument('-?', action='help')

    args = parser.parse_args()
    return parser, args


def main():
    """
    Main entry point for the program.
    """
    (parser, args) = parse_args()
    logging.basicConfig(format='%(levelname)-8s%(message)s')
    ab_config = ABConfig(args)
    try:
        if args.search_by_bounding_box:
            scrape = ABScrapeByBoundingBox(
                ab_config, args.search_by_bounding_box)
            scrape.search(ab_config.FLAGS_ADD)
        elif args.add_and_search_by_bounding_box:
            scrape_id = db_add_scrape(ab_config,
                                      args.add_and_search_by_bounding_box)
            scrape = ABScrapeByBoundingBox(ab_config, scrape_id)
            scrape.search(ab_config.FLAGS_ADD)
        elif args.addsearcharea:
            db_add_search_area(ab_config, args.addsearcharea)
        elif args.add_scrape:
            db_add_scrape(ab_config, args.add_scrape)
        elif args.dbping:
            db_ping(ab_config)
        elif args.delete_scrape:
            db_delete_scrape(ab_config, args.delete_scrape)
        elif args.displayhost:
            display_host(ab_config, args.displayhost)
        elif args.displayroom:
            display_room(ab_config, args.displayroom)
        elif args.listsearcharea:
            list_search_area_info(ab_config, args.listsearcharea)
        elif args.listscrapes:
            list_scrapes(ab_config)
        elif args.printsearch_by_bounding_box:
            scrape = ABScrapeByBoundingBox(
                ab_config, args.printsearch_by_bounding_box)
            scrape.search(ab_config.FLAGS_PRINT)
        else:
            parser.print_help()
    except (SystemExit, KeyboardInterrupt):
        sys.exit()
    except Exception:
        logging.exception("Top level exception handler: quitting.")
        sys.exit(0)


if __name__ == "__main__":
    main()
